import os
import sys
import time
import pandas as pd
import numpy as np
from timeit import default_timer as timer
from sqlalchemy import create_engine

def setup(engine, tablename):
    engine.execute("""DROP TABLE IF EXISTS "%s" """ % (tablename))

    engine.execute("""CREATE TABLE "%s" (
                  "A" INTEGER,
                  "B" INTEGER,
                  "C" INTEGER,
                  "D" INTEGER,
                  CONSTRAINT pk_A_B PRIMARY KEY ("A","B"))
                  """ % (tablename))

if __name__ == '__main__':
    DB_TYPE = 'postgresql'
    DB_DRIVER = 'psycopg2'
    DB_USER = 'admin'
    DB_PASS = 'password'
    DB_HOST = 'localhost'
    DB_PORT = '5432'
    DB_NAME = 'pandas_upsert'
    POOL_SIZE = 50
    TABLENAME = 'test_upsert'
    SQLALCHEMY_DATABASE_URI = '%s+%s://%s:%s@%s:%s/%s' % (DB_TYPE, DB_DRIVER, DB_USER,
                                                          DB_PASS, DB_HOST, DB_PORT, DB_NAME)
    ENGINE = create_engine(
        SQLALCHEMY_DATABASE_URI, pool_size=POOL_SIZE, max_overflow=0)

    print 'setting up db'
    setup(ENGINE, TABLENAME)

    try:
        i=0
        start = timer()
        for i in range(10):
            print 'running test %s' %(str(i))
            df = pd.DataFrame(
                np.random.randint(0, 200, size=(10000, 4)), columns=list('ABCD'))
            df = clean_df_db_dups(df, TABLENAME, ENGINE, dup_cols=['A', 'B'])
            print 'row count after drop db duplicates is now : %s' %(df.shape[0])
            df.to_sql(TABLENAME, ENGINE, if_exists='append', index=False)
            i += 1
        end = timer()
        elapsed_time = end - start
        print 'completed singlethread insert loops in %s sec!' %(elapsed_time)
        inserted = pd.read_sql('SELECT count("A") from %s' %(TABLENAME), ENGINE)
        print 'inserted %s new rows into database!' %(inserted.iloc[0]['count'])

    except KeyboardInterrupt:
        print("Interrupted... exiting...")